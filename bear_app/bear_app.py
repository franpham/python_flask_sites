__author__ = 'franpham'

from flask import Flask, request, flash
from flask import render_template, redirect, url_for, make_response
from options import DEFAULTS

import json
# this app demonstrates using Flask for request routing, setting cookie, and json usage

app = Flask(__name__)
app.secret_key = 'wteiosn09e435089598125dfalhsdsdiuewbvalmcnxvoetio'
# flash sessions are cryptographically signed so must create a random key;

@app.route('/')
def index():
    data = get_saved_data()
    return render_template('index.html', data=data)

@app.route('/save', methods=['POST'])

def save():
    # in Flask, cookies are set on the response so must make_response first;
    flash("Look's great! It's saved!")
    response = make_response(redirect(url_for('builder')))
    data = get_saved_data()
    data.update(dict(request.form.items()))
    response.set_cookie('character', json.dumps(data))
    # data dictionary's value & cookie's value are {'name':value, 'color':value}
    return response

@app.route('/builder')

def builder():
    return render_template('builder.html', data=get_saved_data(), options=DEFAULTS)

def get_saved_data():
    try:
        data = json.loads(request.cookies.get('character'))
    except TypeError:
        data = {}
    return data

app.run(debug=True, port=8000, host='0.0.0.0')