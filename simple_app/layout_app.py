__author__ = 'franpham'

from flask import Flask, render_template

app = Flask(__name__)
# this app demonstrates using Flask for request routing and arguments passing/ retrieval;

@app.route('/')
@app.route('/<name>')
# <> tells route to assign the 1st parameter (after '/', '?' not needed) to name

def index( name="Guest"):
    return "Hello {}".format(name)

@app.route('/add/<int:num1>/<int:num2>')
# The default type of arguments are strings, so must assign variables to int. Note the declaration is <type:name>,
# which is opposite of Python syntax. Must return a string, so return num1 + num2 is invalid.
# If the argument is not parseable to an int, then a 404 response is returned.

def add( num1:int, num2:int):
    context = {'num1':num1, 'num2':num2}
    return render_template('add.html', **context)

@app.route('/multiply')
@app.route('/multiply/<int:num1>/<int:num2>')

def multiply( num1:int=0, num2:int=0):
    # see def add for alternate parameter passing
    return render_template('multiply.html', num1=num1, num2=num2)

app.run( debug=True, port=8000, host='0.0.0.0')
# host '0.0.0.0' tells app to listen on all ports