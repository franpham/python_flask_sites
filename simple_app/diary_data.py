#!/usr/bin/env python3
# for Macs, add above line and type: chmod +x diary_data.py to enable running script by just typing ./diary_data.py

import sys
import datetime
from collections import OrderedDict

from peewee import *

# this app demonstrates using Model's ORM methods with SqliteDatabase;
db = SqliteDatabase('diary.db')

class Entry(Model):
    content = TextField()
    # TextField can be unlimited, but CharField requires max_length

    timestamp = DateTimeField(default = datetime.datetime.now)
    # now is a function, but exclude the () so that it's called whenever an entry is created (else it's called only once)

    class Meta:
        database = db

def initialize():
    db.connect()
    db.create_tables([Entry], safe = True)
    # safe prevents errors if the db or table is already created

def menu_loop():
    choice = None
    while choice != 'q':
        print("Enter 'q' to quit.")
        for key, value in menu.items():
            print("{}: {}".format(key, value.__doc__))
        choice = input("Action: ").lower().strip()
        if choice in menu:
            menu[choice]()

def add_entry():
    """Add an entry."""
    # Ctrl+d is the standard way to terminate reading multiline input
    print("Add your entry, press Ctrl+d when done")
    data = sys.stdin.read().strip()
    if data:
        Entry.create(content=data)
        print("\nEntry saved.")

def delete_entry(entry):
    """Delete an entry."""
    entry.delete_instance()
    print("Entry was deleted.")

def search_entries():
    """Search for content."""
    query = input("Search for?")
    view_entries(query)

def view_entries(query = None):
    """View the entries."""
    entries = Entry.select().order_by(Entry.timestamp.desc())
    if query:
        entries = entries.where(Entry.content.contains(query))

    for entry in entries:
        timestamp = entry.timestamp.strftime("%A, %B %d, %Y %I:%M%p ")
        print(timestamp)
        print(entry.content)
        print("Enter 'd' to delete entry. Enter 'q' to quit. Enter any other letter for next entry.")

        action = input("Action: ")
        if action == 'q':
            break
        elif action == 'd':
            delete_entry(entry)
    print("No more entries.\n")

menu = OrderedDict([
        ('a', add_entry),
        ('v', view_entries),
        ('s', search_entries)
    ])

if __name__ == "__main__":
    initialize()
    menu_loop()