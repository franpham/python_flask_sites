__author__ = 'franpham'

# in Flask, all modules can be imported to the flask.ext namespace
# from flask.ext.bcrypt import generate_password_hash, check_password_hash

from flask_bcrypt import generate_password_hash, check_password_hash
from flask_login import UserMixin
from peewee import *

import datetime

DB = SqliteDatabase('social.db')
# bcrypt uses blowfish encryption that uses a salt so use check_password_hash(hashed_pwd, password) to check equality;

class User(UserMixin, Model):
    # Mixin must be before Model since Model is the base class; UserMixin is required to use flask_login module;
    username = CharField(unique=True)
    email = CharField(unique=True)
    password = CharField(max_length=100)
    joined_at = DateTimeField(default=datetime.datetime.now)
    is_admin = BooleanField(default=False)

    class Meta:
        database = DB
        order_by = ('-joined_at',)
    # ordered_by is a tuple so must use trailing ','
    # '-' before the field specifies to order_by desc instead of the default asc

    def get_posts(self):
        return Post.select().where(Post.user == self)

    def get_stream(self):
        return Post.select().where((Post.user << self.following()), (Post.user == self))
        # the << is an IN SQL operator to test if the data is in a set

    # NOTE: THIS CODE IS DIFFERENT FROM TUTORIAL
    def following(self):
        # query to return who am I following/ reading
        return User.select().join(Relationship, on=Relationship.poster).where(Relationship.reader == self)

    def followers(self):
        # query to return whose following me/ my postings
        return User.select().join(Relationship, on=Relationship.reader).where(Relationship.poster == self)

    @classmethod
    def create_user(cls, username, email, password, admin=False):
        # @classmethod passes in the class reference cls to create an instance
        try:
            # to prevent database from locking if there's an IntegrityError, use: with DB.transaction():
            with DB.transaction():
                return cls.create(username=username, email=email, password=generate_password_hash(password), is_admin=admin)
        except IntegrityError:
            raise ValueError("User already exists.")

class Post(Model):
    timestamp = DateTimeField(default=datetime.datetime.now)
    content = TextField()
    user = ForeignKeyField(
        rel_model=User,
        related_name='posts'
    )
    # related_name is used to refer back to this model, so name it similarly

    class Meta:
        database = DB
        order_by = ('-timestamp',)

class Relationship(Model):
    reader = ForeignKeyField(rel_model=User, related_name='readers')
    poster = ForeignKeyField(rel_model=User, related_name='posters')

    class Meta:
        database = DB
        indexes = ((('reader', 'poster'), True))
        # indexes must be a Tuple (outer ()); each index is a Tuple of fields and boolean for unique index

def initialize():
    DB.connect()
    DB.create_tables([User, Post, Relationship], safe=True)

    try:
        User.create_user(
            username='Francis', email='franpham@gmail.com', password='Py-app1', admin=True)
        # 2nd user: 'Tester', tester@gmail.com, 'Py-app2'
        # 3rd user: 'Tester3', tester3@gmail.com, 'Py-app3'
    except ValueError:
        pass
    DB.close()
