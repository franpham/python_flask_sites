from wtforms import StringField, ValidationError, PasswordField, TextAreaField
from flask_wtf import Form
from flask import g
from wtforms.validators import DataRequired, Regexp, Email, Length, EqualTo
from flask_login import login_user

from models import User

def name_exists(form, field):
    if User.select().where(User.username == field.data).exists():
        if g.user.is_authenticated() and g.user.username == field.data:
            pass
        else:
            raise ValidationError("Username already exists.")

def email_exists(form, field):
    if User.select().where(User.email == field.data).exists():
        if g.user.is_authenticated() and g.user.email == field.data:
            pass
        else:
            raise ValidationError("Email already exists.")

class RegisterForm(Form):
    # Field's validators takes functions to call for validation
    username = StringField(label='Username',
                           validators=[DataRequired(),
                            Regexp(r'^[a-zA-Z0-9_]+$', message=("Username should be letters, numbers, and underscore only.")),
                            name_exists])   # NOTE: module functions have (), but custom functions do not!

    email = StringField(label='Email', validators=[DataRequired(), Email(), email_exists])
    password = PasswordField(label='Password', validators=[DataRequired(), Length(min=4), EqualTo('password2', message='Passwords must match.')])
    password2 = PasswordField(label='Confirm Password', validators=[DataRequired()])

    # this is not needed, but included to show how to call the super method of constructors;
    def __init__(self, **kwargs):
        super(RegisterForm, self).__init__(**kwargs)

    def updateFields(self, user:User):
        self.username.data = user.username
        self.email.data = user.email
        self.password.data = user.password

class LoginForm(Form):
    email = StringField(label='Email', validators=[DataRequired(), Email()])
    password = PasswordField(label='Password', validators=[DataRequired()])

class PostForm(Form):
    content = TextAreaField(label="What's your status?", validators=[DataRequired()])