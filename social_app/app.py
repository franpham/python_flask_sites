__author__ = 'franpham'

from flask import Flask, g, flash, redirect, url_for, render_template, abort, request
# g contains global variables to pass info between view and modules

from flask_login import LoginManager, login_user, logout_user, login_required, current_user
from flask_bcrypt import check_password_hash, generate_password_hash
# in Flask, all modules can be imported to the flask.ext namespace

import forms
import models

DEBUG = True
PORT = 8000
HOST = '0.0.0.0'

app = Flask(__name__)
app.secret_key = 'dsfsdoinvwe05243589hdlksad;lkj]\[;'

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

@app.before_request
def before_request():
    # Connect to the database before each request
    g.db = models.DB
    g.db.connect()
    g.user = current_user
    # NOTE: current_user is lazily loaded so must call _get_current_object()

@app.after_request
def after_request(response):
    # Close the database after each request
    g.db.close()
    return response

@app.route('/update_user', methods=('GET', 'POST'))
@login_required
def update_user():
    form = forms.RegisterForm()
    if request.method == 'GET':
        form.updateFields(user=g.user)
    elif form.validate_on_submit():
        try:
            user = models.User.get(models.User.id == g.user.get_id())
            user.username = form.username.data
            user.email = form.email.data
            user.password = generate_password_hash(form.password.data)
            user.save()
        except models.IntegrityError:
            pass
        else:
            return redirect(url_for('index'))
    return render_template('register.html', form=form)

@app.route('/register', methods=('GET', 'POST'))
def register():
    form = forms.RegisterForm()
    if form.validate_on_submit():
        try:
            user = models.User.create_user(username=form.username.data, email=form.email.data, password=form.password.data)
            login_user(user)
            flash("Thanks for registering!", 'success')
        except ValueError:
            flash("User already exists.", 'error')
        else:
            return redirect(url_for('index'))
    return render_template('register.html', form=form)

@app.route('/login', methods=('GET', 'POST'))
def login():
    form = forms.LoginForm()
    if form.validate_on_submit():
        try:
            user = models.User.get(models.User.email == form.email.data)
            if check_password_hash(user.password, form.password.data):
                login_user(user)
                # login_user creates the user's session and set the session cookie;

                flash("You're logged in!", 'success')
                return redirect(url_for('index'))
            else:
                flash("Your email or password does not match!", 'error')
        except models.DoesNotExist:
            flash("The email or password does not match!", 'error')
    return render_template('login.html', form=form)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash("You've been logged out.", 'success')
    return redirect(url_for('index'))

@app.route('/')
def index():
    if g.user.is_authenticated():
        stream = models.Post.select().limit(100)
    else:
        stream = []
    return render_template('stream.html', stream=stream)

@app.route('/post/<int:post_id>')
def view_post(post_id):
    posts = models.Post.select().where(models.Post.id == post_id)
    if posts.count() == 0:
        abort(404)
    return render_template('stream.html', stream=posts)

@app.route('/stream')
@app.route('/stream/<username>')
@login_required
def stream(username=None):
    if username and username != g.user.username:
        try:
            user = models.User.select().where(models.User.username ** username).get()
            stream = user.posts.limit(100)
            # peewee syntax: ** is a LIKE SQL operator that ignores case
            # NOTE: user references posts since foreign table "posts" was specified in the ForeignKeyField
        except models.DoesNotExist:
            abort(404)
    else:
        user = g.user
        stream = user.get_stream().limit(100)
    return render_template('user_stream.html', stream=stream, user=user)

@app.route('/new_post', methods=('GET', 'POST'))
@login_required
def post():
    form = forms.PostForm()
    if form.validate_on_submit():
        # _get_current_object() was called in before_request()
        models.Post.create(user=g.user._get_current_object(), content=form.content.data.strip())
        flash("Your message was posted!", 'success')
        return redirect(url_for('index'))
    return render_template('post.html', form=form)
    # use name=value to pass data to html via render_template

@app.route('/follow/<username>')
@login_required
def follow(username):
    try:
        poster = models.User.get(models.User.username ** username)
    except models.DoesNotExist:
        pass
    else:
        try:
            models.Relationship.create(reader=g.user._get_current_object(), poster=poster)
        except models.IntegrityError:
            pass
        else:
            flash("You're now following {}".format(poster.username), 'success')
    return redirect(url_for('stream', username=poster.username))

@app.route('/unfollow/<username>')
@login_required
def unfollow(username):
    try:
        poster = models.User.get(models.User.username ** username)
    except models.DoesNotExist:
        pass
    else:
        try:
            models.Relationship.get(reader=g.user._get_current_object(), poster=poster).delete_instance()
        except models.IntegrityError:
            pass
        else:
            flash("You've unfollowed {}".format(poster.username), 'success')
    return redirect(url_for('stream', username=poster.username))

@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404

# the method decorator user_loader denotes the callback to load a user using a session ID;
@login_manager.user_loader
def load_user(userid):
    try:
        return models.User.get(models.User.id == userid)
    except models.DoesNotExist:
        return None

if __name__ == '__main__':
    models.initialize()
    app.run(debug=DEBUG, host=HOST, port=PORT)